package com.danilo.vargas.adspoint.Objetos;


import java.io.Serializable;

public class Local implements Serializable {
    private long mId;
    private String mHeader;
    private String mNombre;
    private String mTelefono;
    private String mCorreo;
    private String mDescripcion;
    private String mUbicacion;

    public Local(long pId, String pHeader, String pNombre, String pTelefono, String pCorreo,
                 String pDescripcion, String pUbicacion) {
        mId = pId;
        mHeader = pHeader;
        mNombre = pNombre;
        mTelefono = pTelefono;
        mCorreo = pCorreo;
        mDescripcion = pDescripcion;
        mUbicacion = pUbicacion;
    }

    public long getmId() {
        return mId;
    }

    public void setmId(long pId) {
        mId = pId;
    }

    public String getmHeader() {
        return mHeader;
    }

    public void setmHeader(String pHeader) {
        mHeader = pHeader;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String pNombre) {
        mNombre = pNombre;
    }

    public String getmTelefono() {
        return mTelefono;
    }

    public void setmTelefono(String pTelefono) {
        mTelefono = pTelefono;
    }

    public String getmCorreo() {
        return mCorreo;
    }

    public void setmCorreo(String pCorreo) {
        mCorreo = pCorreo;
    }

    public String getmDescripcion() {
        return mDescripcion;
    }

    public void setmDescripcion(String pDescripcion) {
        mDescripcion = pDescripcion;
    }

    public String getmUbicacion() {
        return mUbicacion;
    }

    public void setmUbicacion(String pUbicacion) {
        mUbicacion = pUbicacion;
    }
}
