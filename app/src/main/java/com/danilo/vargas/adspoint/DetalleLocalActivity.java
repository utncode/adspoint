package com.danilo.vargas.adspoint;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Objetos.Promocion;
import com.danilo.vargas.adspoint.datos.Locales;

import java.util.ArrayList;


public class DetalleLocalActivity extends AppCompatActivity {
    public static final String EXTRA_NAME = "cheese_name";
    Local local;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle_local);
        Locales locales = new Locales(this);
        locales.open();

        Intent intent = getIntent();
        final String idLocal = intent.getStringExtra(EXTRA_NAME);
        local = locales.obtenerLocal(idLocal);
        locales.close();


        //este tipo de toolbar es el que permite que se pueda ocultar la info cuando bajamos el scroll
        CollapsingToolbarLayout collapsingToolbar =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setTitle(local.getmNombre());



        TextView telefono = (TextView) findViewById(R.id.telefono);
       TextView correo = (TextView) findViewById(R.id.correo);
        TextView ubicacion = (TextView) findViewById(R.id.ubicacion);
        TextView descripcion = (TextView) findViewById(R.id.descripcion);

        telefono.setText(local.getmTelefono());
        correo.setText(local.getmCorreo());
        ubicacion.setText(local.getmUbicacion());
        descripcion.setText(local.getmDescripcion());


        setupRecyclerView(idLocal);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setupRecyclerView(String pIdLocal) {

        //Obtenemos las promociones de ese local para cargarlas en la lista de promociones
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        com.danilo.vargas.adspoint.datos.Promociones promos =
                new com.danilo.vargas.adspoint.datos.Promociones(this);
        promos.open();
        recyclerView.setAdapter(new SimpleStringRecyclerViewAdapter(this,
                promos.ObtenerPromocionesLocal(pIdLocal)));
        promos.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private ArrayList<Promocion> mValues;
        private final Context mContext;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public String mUsuarioString;
            public String mComentarioString;

            public final View mView;
            public final TextView mNombre;
            public final TextView mComentario;
            public final CardView mCard;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mNombre = (TextView) view.findViewById(R.id.usuario);
                mComentario = (TextView) view.findViewById(R.id.comentario);
                mCard = (CardView) view.findViewById(R.id.card);

            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNombre.getText();
            }
        }

        public SimpleStringRecyclerViewAdapter(Context context, ArrayList<Promocion> items) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mValues = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_comentarios, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Promocion promocion =  mValues.get(position);
            holder.mUsuarioString = promocion.getmNombreLocal();
            holder.mComentarioString = promocion.getMensaje();
            holder.mNombre.setText(holder.mUsuarioString );
            holder.mComentario.setText(holder.mComentarioString);
            if (promocion.ismActvo()){
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.verde_claro));
            } else {
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.celeste));
            }
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }
    }

    public void nuevoComentario(View view) {
        Intent intent = new Intent(this, ComentarioNuevoActivity.class);
        intent.putExtra("local", local);
        startActivity(intent);
    }
}
