package com.danilo.vargas.adspoint.datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.danilo.vargas.adspoint.Objetos.Promocion;
import com.danilo.vargas.adspoint.Utils.ObtenerTimestamp;

import java.util.ArrayList;

public class Promociones {
    private static final int ID = 0;
    private static final int HEADER = 1;
    private static final int TITULO = 2;
    private static final int MENSAJE = 3;
    private static final int FECHA_CADUCIDAD = 4;
    private static final int ID_LOCAL = 5;
    private static final int ACTIVO = 6;
    private static final int CATEGORIA = 7;
    private static final String FORMATO_IGUAL = "%s = ?";

    private final Context mContext;
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    private String[] allColumns = {
            DBHelper.PromocionDB.ID,
            DBHelper.PromocionDB.HEADER,
            DBHelper.PromocionDB.TITULO,
            DBHelper.PromocionDB.MENSAJE,
            DBHelper.PromocionDB.FECHA_CADUCIDAD,
            DBHelper.PromocionDB.ID_LOCAL,
            DBHelper.PromocionDB.ACTIVO,
            DBHelper.PromocionDB.CATEGORIA};

    public Promociones(Context context) {
        dbHelper = new DBHelper(context);
        mContext = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Promocion guardarPromocion(Promocion pPromocion) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.PromocionDB.ID_LOCAL, pPromocion.getIdLocal());
        values.put(DBHelper.PromocionDB.HEADER, pPromocion.getHeader());
        values.put(DBHelper.PromocionDB.TITULO, pPromocion.getTitulo());
        values.put(DBHelper.PromocionDB.FECHA_CADUCIDAD, pPromocion.getfechaCaducidad());
        values.put(DBHelper.PromocionDB.MENSAJE, pPromocion.getMensaje());
        values.put(DBHelper.PromocionDB.CATEGORIA, pPromocion.getmCategoria());
        values.put(DBHelper.PromocionDB.ACTIVO, true);
        values.put(DBHelper.CREATED_AT, ObtenerTimestamp.obtenerTimestamp());
        values.put(DBHelper.UPDATE_AT, ObtenerTimestamp.obtenerTimestamp());
        long insertId = database.insert(DBHelper.PromocionDB.NOMBRE_TABLA, null,
                values);
        pPromocion.setId(insertId);

        return pPromocion;
    }

    public ArrayList<Promocion> ObtenerPromociones(){
            ArrayList<Promocion> promociones = new ArrayList<>();


        String sortOrder =
                DBHelper.PromocionDB.ID + " DESC";
            Cursor cursor = database.query(DBHelper.PromocionDB.NOMBRE_TABLA,
                    allColumns, null, null, null, null, sortOrder,null);

        Locales locales = new Locales(mContext);
        locales.open();
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                promociones.add(cursorPromocion(cursor, locales));
                cursor.moveToNext();
            }

            cursor.close();
        locales.close();

        return promociones;
    }

    public ArrayList<Promocion> ObtenerPromocionesLocal(String pHeader){
        ArrayList<Promocion> promociones = new ArrayList<>();

        String sql =  String.format(FORMATO_IGUAL, DBHelper.PromocionDB.ID_LOCAL);


        String sortOrder =
                DBHelper.PromocionDB.ID + " DESC";
        Cursor cursor = database.query(DBHelper.PromocionDB.NOMBRE_TABLA,
                allColumns, sql, new String[] { pHeader },
                null, null, sortOrder, null);

        Locales locales = new Locales(mContext);
        locales.open();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            promociones.add(cursorPromocion(cursor, locales));
            cursor.moveToNext();
        }

        cursor.close();
        locales.close();

        return promociones;
    }

    public ArrayList<String> ObtenerHeaders(){
        ArrayList<String> headers = new ArrayList<>();
        Cursor cursor = database.query(DBHelper.PromocionDB.NOMBRE_TABLA,
                new String[]{DBHelper.PromocionDB.HEADER}, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            headers.add(cursor.getString(cursor.getColumnIndex(DBHelper.PromocionDB.HEADER)));
            cursor.moveToNext();
        }

        cursor.close();

        return headers;
    }

    private Promocion cursorPromocion(Cursor pCursor, Locales pLocales){
        String local = pLocales.obtenerNombreLocal(pCursor.getString(ID_LOCAL));
        boolean activo = pCursor.getInt(ACTIVO) == 1;

        return  new Promocion(pCursor.getLong(ID),
                pCursor.getString(HEADER),
                pCursor.getString(TITULO),
                pCursor.getString(ID_LOCAL),
                pCursor.getString(FECHA_CADUCIDAD),
                pCursor.getString(MENSAJE),
                local,
                activo,
                pCursor.getString(CATEGORIA));
    }

    public void desactivar(){
        ContentValues values = new ContentValues();
        values.put(DBHelper.PromocionDB.ACTIVO, false);
        database.update(DBHelper.PromocionDB.NOMBRE_TABLA, values, null, null);
    }
}