package com.danilo.vargas.adspoint.servicio;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.danilo.vargas.adspoint.MainActivity;
import com.danilo.vargas.adspoint.R;
import com.danilo.vargas.adspoint.conexion.AppController;
import com.danilo.vargas.adspoint.datos.Comentarios;
import com.danilo.vargas.adspoint.datos.Locales;
import com.danilo.vargas.adspoint.datos.Promociones;
import com.danilo.vargas.adspoint.logica.Json;
import com.danilo.vargas.adspoint.logica.VerificarDatosExistentes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* Esta clase dispara el metodo onReceive cuando hay un cambio de red en el telefono
* */

public class AutoArranque extends BroadcastReceiver {
    public static final int NOTIFICATION_ID = 1;
    public static final String URL_FORMAT = "http://%s/cliente";
    public static final String CANTIDAD_FORMAT = "%s Promociones Nuevas";
    public static final String TEXTO_PROMO_FORMAT = "%s (%s)";
    private static boolean firstConnect = true;

    /*En este metodo validamos que la conexion se wifi y no datos moviles, ademas de validar lo que lo
     disparo no fuera el apagado del wifi
     Si todo es correcto llamamos al metodo verificarServicio*/
    @Override
    public void onReceive(final Context context, Intent intent) {
        final ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetInfo != null) {
            boolean isWiFi = activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI;
            if(firstConnect && isWiFi) {
                verificarServicio(context);
                firstConnect = false;
            } else {
                if (!isWiFi) {
                    firstConnect= true;
                }
            }
        }
        else {
            firstConnect= true;
        }

    }

    /*En este metodo hacemos la peticion al dispositivo que nos da la señal para vericar que posea
    * el servicio*/
    public void verificarServicio(final Context context) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isWiFi = false;

        if (activeNetwork != null){
            isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        }

        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        DhcpInfo d = wifi.getDhcpInfo();
        int s_gateway = d.gateway;
        //validamos nuevamente nuestra conexon
        if (isWiFi) {

            //Tomamos la puerta de enlace para poder hacer nuestra consulta
            String url = String.format(URL_FORMAT, intToIp(s_gateway));
            /*Abrimos las conexiones para la tablas y asi obtener todos los registros que poseemos
            en nuestra base de datos */
            Promociones promociones = new Promociones(context);
            Locales locales = new Locales(context);
            Comentarios comentarios = new Comentarios(context);
            promociones.open();
            locales.open();
            comentarios.open();

            /*En esta linea de codigo en la clase Json en en paquete logica generamos el JSONObject
            o json que le vamos a enviar al servidor por medio de de la peticion JsonObjectRequest
             */
            JSONObject json = Json.crearJson( promociones.ObtenerPromociones(),
                    comentarios.ObtenerComentarios(),
            locales.ObtenerLocales());

            //Cerramos las instancias a la base de datos
            promociones.close();
            locales.close();
            comentarios.close();

            //Realizamos la peticion al servidor utiizando JsonObjectRequest de la libreria Volley

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, json,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            //Validamos y verificamos nuestros datos con los que nos envio el servicio
                            VerificarDatosExistentes vf = new VerificarDatosExistentes(context);
                            vf.nuevasLocalesCliente(response);
                            vf.nuevosComentariosCliente(response);
                            //Si encontramos que hay nuevas promociones para nosotros notificamos al usuario
                            JSONArray nuevas = vf.nuevasPromocionesCliente(response);
                            if (nuevas.length() > 0){
                                notificarUsuario(context, nuevas);
                            }


                            vf.close();


                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //Si sucede un error no hacemos nada (posiblemente el host no tenga el servicio necesario)
                }
            }) {

            };
            AppController.getInstance().addToRequestQueue(jsonObjReq);
        }
    }

    //En este metodo generamos las notificacion para el usuario
    private void notificarUsuario(Context pContext, JSONArray response) {

        String nombreApp = pContext.getResources().getString(R.string.app_name);
        Intent intent = new Intent(pContext, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(pContext, 0, intent, 0);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(pContext);
        builder.setSmallIcon(R.mipmap.ic_logo);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(pContext.getResources(), R.mipmap.ic_logo));
        builder.setContentTitle(nombreApp);
        builder.setContentText(nombreApp);
        builder.setDefaults(Notification.DEFAULT_SOUND);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(nombreApp);
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject person  = (JSONObject) response.get(i);

                inboxStyle.addLine(
                        String.format(TEXTO_PROMO_FORMAT, person.getString("titulo"),
                                person.getString("mensaje")));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        builder.setSubText(String.format(CANTIDAD_FORMAT, response.length()));
        builder.setStyle(inboxStyle);
        NotificationManager notificationManager = (NotificationManager) pContext.getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public String intToIp(int i) {

        return (i & 0xFF) + "." +
                ((i >> 8) & 0xFF) + "." +
                ((i >> 16) & 0xFF) + "." +
                ((i >> 24) & 0xFF);
    }
}
