package com.danilo.vargas.adspoint.logica;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Objetos.Promocion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Json {
    public static final String HEADER = "header";
    public static final String LOCAL = "local";
    public static final String COMENTARIO = "comentario";
    public static final String FECHA = "fecha";
    public static final String USUARIO = "usuario";
    public static final String GRUPO = "grupo";

    public static final String MENSAJE = "mensaje";
    public static final String FECHA_EXPIRACION = "fecha_expiracion";
    public static final String TITULO = "titulo";
    public static final String CATEGORIA = "categoria";

    public static final String NOMBRE = "nombre";
    public static final String TELEFONO = "telefono";
    public static final String CORREO = "correo";
    public static final String DESCRIPCION = "descripcion";
    public static final String UBICACION = "ubicacion";

    public static final String COMENTARIOS_PARENT = "comentarios";
    public static final String PROMOCIONES_PARENT = "promociones";
    public static final String LOCALES_PARENT = "locales";
    public static final String BODY = "body";



//Este metodo los uyilizamos para crear el JSONObject que ira en la peticion al servidor
    public static JSONObject crearJson(ArrayList<Promocion> pPromociones,
                                       ArrayList<Comentario> pComentarios,
                                       ArrayList<Local> pLocales){
        JSONObject finalparent = new JSONObject();
        JSONObject parent = new JSONObject();
        JSONObject obj;

        JSONArray arrayComentario = new JSONArray();
        for (Comentario comentario :pComentarios)
            try {

                obj = new JSONObject();

                obj.put(LOCAL, comentario.getIdLocal());
                obj.put(COMENTARIO, comentario.getComentario());
                obj.put(FECHA, comentario.getFecha());
                obj.put(USUARIO, comentario.getUsuario());
                obj.put(GRUPO, comentario.getmIdGrupo());
                obj.put(HEADER, comentario.getHeader());

                arrayComentario.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        JSONArray arrayPromociones = new JSONArray();
        for (Promocion promociones :pPromociones)
            try {

                obj = new JSONObject();

                obj.put(LOCAL, promociones.getIdLocal());
                obj.put(MENSAJE, promociones.getMensaje());
                obj.put(FECHA_EXPIRACION, promociones.getfechaCaducidad());
                obj.put(TITULO, promociones.getTitulo());
                obj.put(HEADER, promociones.getHeader());
                obj.put(CATEGORIA, promociones.getmCategoria());

                arrayPromociones.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        JSONArray arrayLocales = new JSONArray();
        for (Local local :pLocales){

            try {

                obj = new JSONObject();

                obj.put(HEADER, local.getmHeader());
                obj.put(NOMBRE, local.getmNombre());
                obj.put(TELEFONO, local.getmTelefono());
                obj.put(CORREO, local.getmCorreo());
                obj.put(DESCRIPCION, local.getmDescripcion());
                obj.put(UBICACION, local.getmUbicacion());


                arrayLocales.put(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            parent.put(COMENTARIOS_PARENT, arrayComentario);
            parent.put(PROMOCIONES_PARENT, arrayPromociones);
            parent.put(LOCALES_PARENT, arrayLocales);
            finalparent.put(BODY, parent);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return finalparent;
    }
}
