package com.danilo.vargas.adspoint.servicio;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.danilo.vargas.adspoint.Objetos.Grupo;
import com.danilo.vargas.adspoint.datos.Grupos;

public class RecibirMensaje extends BroadcastReceiver {

    /*En esta clase instervenimos los msj y validamos si es msj con el
    formato para nuestra aplicacion si lo posee guardamos el nuevo grupo
    en nuestra base de datos
    */
    @Override
    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                String mensaje = "";
                for (Object aPdusObj : pdusObj) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String message = currentMessage.getDisplayMessageBody();
                    mensaje += message;

                }

                String[] datos = mensaje.split("\n");

                if (datos.length == 3){
                    if (datos[0].equals("AdsPoint")){
                        Grupos grupos = new Grupos(context);
                        grupos.open();
                        grupos.guardarGrupo(new Grupo(0, datos[1],datos[2]));
                    }
                }
            }

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" +e);

        }
    }
}