package com.danilo.vargas.adspoint.datos;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public class DBHelper extends SQLiteOpenHelper {

    /*
    Esta es la clase principal para el manejo de la base de datos cada una de esas clases son
    los modelos de la tablas de la base de datos

    * */
    public static final int VERSION_BASEDATOS = 1;
    public static final String NOMBRE_BASEDATOS = "AdsPoint.db";
    private static final String TIPO_TEXTO = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String TIMESTAMP = " TIMESTAMP";
    private static final String BOOLEAN = " BOOLEAN";
    private static final String INTEGER = " INTEGER";
    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String FOREIGN_KEY = " FOREIGN KEY(%s) REFERENCES %s(%s)";
    private static final String CREAR_TABLA = "CREATE TABLE ";
    private static final String INICIAR_TABLA = "(";
    private static final String TERMINAR_TABLA = ")";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATE_AT = "update_at";

    //Aqui en este contructor iniciamos la conexion con la base de datos
    public DBHelper(Context context) {
        super(context, NOMBRE_BASEDATOS, null, VERSION_BASEDATOS);
    }

    //Este metodo se ejecuta automaticamente cuando no existe un base de datos para la aplicacio
    //Si ya esta creada no se vuelve a utilizar
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LocalBD.SQL_CREAR_TABLA);
        db.execSQL(PromocionDB.SQL_CREAR_TABLA);
        db.execSQL(GrupoDB.SQL_CREAR_TABLA);
        db.execSQL(ComentarioBD.SQL_CREAR_TABLA);

    }

    //Este metodo funciona para actulizar tablas o campos de tabla en la base de datos
    //Depende si cambiamos la version de la base de datos a la hora de inicializarlo en el constructor de esta clase
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /*Esta seria la tabla promocion aqui le adjuntamos nombres para las columnas y un string para
    crear la tabla, este string es necesario para llamarlo en el onCreate()
    */
    public static abstract class PromocionDB implements BaseColumns {
        private static final String FORMATO = "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" +
                "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s";
        public static final String NOMBRE_TABLA = "promocion";
        public static final String ID = "id";
        public static final String HEADER = "header";
        public static final String TITULO = "titulo";
        public static final String ID_LOCAL = "id_local";
        public static final String FECHA_CADUCIDAD = "fecha_caducidad";
        public static final String MENSAJE = "mensaje";
        public static final String CATEGORIA = "categoria";
        public static final String ACTIVO = "activo";
        public static final String FOREIGN_KEY_LOCAL =
                String.format(FOREIGN_KEY, ID_LOCAL, LocalBD.NOMBRE_TABLA, LocalBD.HEADER);

        private static final String SQL_CREAR_TABLA =
                String.format(FORMATO, CREAR_TABLA, NOMBRE_TABLA, INICIAR_TABLA,
                        ID, INTEGER, PRIMARY_KEY, COMMA_SEP,
                        ID_LOCAL, TIPO_TEXTO, COMMA_SEP,
                        HEADER, TIPO_TEXTO, COMMA_SEP,
                        TITULO, TIPO_TEXTO, COMMA_SEP,
                        MENSAJE, TIPO_TEXTO, COMMA_SEP,
                        CATEGORIA, TIPO_TEXTO, COMMA_SEP,
                        FECHA_CADUCIDAD, TIMESTAMP, COMMA_SEP,
                        ACTIVO, BOOLEAN, COMMA_SEP,
                        CREATED_AT, TIMESTAMP, COMMA_SEP,
                        UPDATE_AT, TIMESTAMP,COMMA_SEP,
                        FOREIGN_KEY_LOCAL, TERMINAR_TABLA);
    }

    /*Esta seria la tabla comentario aqui le adjuntamos nombres para las columnas y un string para
   crear la tabla, este string es necesario para llamarlo en el onCreate()
   */
    public static abstract class ComentarioBD implements BaseColumns {
        private static final String FORMATO = "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" +
                "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s";
        public static final String NOMBRE_TABLA = "comentario";
        public static final String ID = "id";
        public static final String HEADER = "header";
        public static final String USUARIO = "usuario";
        public static final String FECHA = "fecha";
        public static final String COMENTARIO = "comentario";
        public static final String ID_LOCAL = "id_local";
        public static final String ID_GRUPO = "id_gupo";
        public static final String ACTIVO = "activo";
        public static final String FOREIGN_KEY_LOCAL =
                String.format(FOREIGN_KEY, ID_LOCAL, LocalBD.NOMBRE_TABLA, LocalBD.HEADER);

        private static final String SQL_CREAR_TABLA =
                String.format(FORMATO, CREAR_TABLA, NOMBRE_TABLA, INICIAR_TABLA,
                        ID, INTEGER, PRIMARY_KEY, COMMA_SEP,
                        ID_LOCAL, TIPO_TEXTO, COMMA_SEP,
                        HEADER, TIPO_TEXTO, COMMA_SEP,
                        USUARIO, TIPO_TEXTO, COMMA_SEP,
                        COMENTARIO, TIPO_TEXTO, COMMA_SEP,
                        ID_GRUPO, TIPO_TEXTO, COMMA_SEP,
                        FECHA, TIMESTAMP, COMMA_SEP,
                        ACTIVO, BOOLEAN, COMMA_SEP,
                        CREATED_AT, TIMESTAMP, COMMA_SEP,
                        UPDATE_AT, TIMESTAMP, COMMA_SEP,
                        FOREIGN_KEY_LOCAL, TERMINAR_TABLA);
    }

    /*Esta seria la tabla local aqui le adjuntamos nombres para las columnas y un string para
   crear la tabla, este string es necesario para llamarlo en el onCreate()
   */
    public static abstract class LocalBD implements BaseColumns {
        private static final String FORMATO = "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" +
                "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s";
        public static final String NOMBRE_TABLA = "local";
        public static final String ID = "id";
        public static final String HEADER = "header";
        public static final String NOMBRE = "nombre";
        public static final String TELEFONO = "telefono";
        public static final String CORREO = "correo";
        public static final String DESCRIPCION = "descrpcion";
        public static final String UBICACION = "ubicacion";
        public static final String ACTIVO = "activo";

        private static final String SQL_CREAR_TABLA =
                String.format(FORMATO, CREAR_TABLA, NOMBRE_TABLA, INICIAR_TABLA,
                        ID, INTEGER, PRIMARY_KEY, COMMA_SEP,
                        HEADER, TIPO_TEXTO, COMMA_SEP,
                        NOMBRE, TIPO_TEXTO, COMMA_SEP,
                        TELEFONO, TIPO_TEXTO, COMMA_SEP,
                        CORREO, TIPO_TEXTO, COMMA_SEP,
                        DESCRIPCION, TIPO_TEXTO, COMMA_SEP,
                        UBICACION, TIPO_TEXTO, COMMA_SEP,
                        ACTIVO, BOOLEAN, COMMA_SEP,
                        CREATED_AT, TIMESTAMP, COMMA_SEP,
                        UPDATE_AT, TIMESTAMP, TERMINAR_TABLA);
    }

    /*Esta seria la tabla grupos aqui le adjuntamos nombres para las columnas y un string para
   crear la tabla, este string es necesario para llamarlo en el onCreate()
   */
    public static abstract class GrupoDB implements BaseColumns {
        private static final String FORMATO = "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s" +
                "%s %s %s %s %s %s";
        public static final String NOMBRE_TABLA = "grupo";
        public static final String ID = "id";
        public static final String HEADER = "header";
        public static final String NOMBRE = "nombre";
        public static final String ACTIVO = "activo";

        private static final String SQL_CREAR_TABLA =
                String.format(FORMATO, CREAR_TABLA, NOMBRE_TABLA, INICIAR_TABLA,
                        ID, INTEGER, PRIMARY_KEY, COMMA_SEP,
                        HEADER, TIPO_TEXTO, COMMA_SEP,
                        NOMBRE, TIPO_TEXTO, COMMA_SEP,
                        ACTIVO, BOOLEAN, COMMA_SEP,
                        CREATED_AT, TIMESTAMP, COMMA_SEP,
                        UPDATE_AT, TIMESTAMP, TERMINAR_TABLA);
    }


}