package com.danilo.vargas.adspoint.datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Utils.ObtenerTimestamp;

import java.util.ArrayList;

public class Locales {
    private static final int ID = 0;
    private static final int HEADER = 1;
    private static final int NOMBRE = 2;
    private static final int TELEFONO = 3;
    private static final int CORREO = 4;
    private static final int DESCRIPCION = 5;
    private static final int UBICACION = 6;

    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private static String FORMATO_IGUAL = "%s = ?";
    private String[] allColumns = {
            DBHelper.LocalBD.ID,
            DBHelper.LocalBD.HEADER,
            DBHelper.LocalBD.NOMBRE,
            DBHelper.LocalBD.TELEFONO,
            DBHelper.LocalBD.CORREO,
            DBHelper.LocalBD.DESCRIPCION,
            DBHelper.LocalBD.UBICACION,
            DBHelper.LocalBD.ACTIVO};

    public Locales(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();

    }

    public void close() {
        dbHelper.close();
    }

    public Local guardarPromocion(Local pLocal) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.LocalBD.NOMBRE, pLocal.getmNombre());
        values.put(DBHelper.LocalBD.HEADER, pLocal.getmHeader());
        values.put(DBHelper.LocalBD.TELEFONO, pLocal.getmTelefono());
        values.put(DBHelper.LocalBD.CORREO, pLocal.getmCorreo());
        values.put(DBHelper.LocalBD.DESCRIPCION, pLocal.getmDescripcion());
        values.put(DBHelper.LocalBD.UBICACION, pLocal.getmUbicacion());
        values.put(DBHelper.PromocionDB.ACTIVO, true);
        values.put(DBHelper.CREATED_AT, ObtenerTimestamp.obtenerTimestamp());
        values.put(DBHelper.UPDATE_AT, ObtenerTimestamp.obtenerTimestamp());
        long insertId = database.insert(DBHelper.LocalBD.NOMBRE_TABLA, null,
                values);
        pLocal.setmId(insertId);

        return pLocal;
    }

    public String obtenerNombreLocal(String pIdLocal){
    String nombre = "";
        String sql =  String.format(FORMATO_IGUAL, DBHelper.LocalBD.HEADER);
        Cursor cursor = database.query(DBHelper.LocalBD.NOMBRE_TABLA,
                new String[]{DBHelper.LocalBD.NOMBRE,}, sql, new String[] { pIdLocal },
                null, null, null, null);

        cursor.moveToFirst();
        if(cursor.moveToFirst()){

            nombre = cursor.getString(ID);
        }
        cursor.close();

        return nombre;
    }

    public Local obtenerLocal(String pIdLocal){
       Local local = null;
        String sql =  String.format(FORMATO_IGUAL, DBHelper.LocalBD.HEADER);
        Cursor cursor = database.query(DBHelper.LocalBD.NOMBRE_TABLA,
                allColumns, sql, new String[] { pIdLocal },
                null, null, null, null);

        cursor.moveToFirst();
        if(cursor.moveToFirst()){

            local = cursorLocal(cursor);
        }
        cursor.close();

        return local;
    }

    public ArrayList<Local> ObtenerLocales(){
        ArrayList<Local> locales = new ArrayList<>();

        Cursor cursor = database.query(DBHelper.LocalBD.NOMBRE_TABLA,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            locales.add(cursorLocal(cursor));
            cursor.moveToNext();
        }

        cursor.close();

        return locales;
    }

    public ArrayList<String> ObtenerHeaders(){
        ArrayList<String> headers = new ArrayList<>();
        Cursor cursor = database.query(DBHelper.LocalBD.NOMBRE_TABLA,
                new String[]{DBHelper.LocalBD.HEADER}, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            headers.add(cursor.getString(cursor.getColumnIndex(DBHelper.LocalBD.HEADER)));
            cursor.moveToNext();
        }

        cursor.close();

        return headers;
    }



    private Local cursorLocal(Cursor pCursor){

        return  new Local(pCursor.getLong(ID), pCursor.getString(HEADER),
                pCursor.getString(NOMBRE), pCursor.getString(TELEFONO),
                pCursor.getString(CORREO), pCursor.getString(DESCRIPCION),
                pCursor.getString(UBICACION));
    }

}

