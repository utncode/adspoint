package com.danilo.vargas.adspoint.Utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class ObtenerTimestamp {
    //Obtenemos la fecha actual para guardar
    public static String obtenerTimestamp() {
        Calendar calendar = Calendar.getInstance();
        Timestamp fecha = new Timestamp(calendar.getTime().getTime());
        String fechaString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(fecha);
        return fechaString;
    }

}