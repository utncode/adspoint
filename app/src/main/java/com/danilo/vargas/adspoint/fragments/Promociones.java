package com.danilo.vargas.adspoint.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.danilo.vargas.adspoint.DetalleLocalActivity;
import com.danilo.vargas.adspoint.Objetos.Promocion;
import com.danilo.vargas.adspoint.R;

import java.util.ArrayList;
import java.util.Iterator;

public class Promociones extends Fragment{
    /*
      Esta clase es una vista dentro del activity donde cargamos las promociones en la
      vista por medio de una lista que esta alojada en el RecyclerView
  * */
    ArrayList<Promocion> promociones;
    SimpleStringRecyclerViewAdapter adapter;
    Spinner spinner;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LinearLayout ln = (LinearLayout) inflater.inflate(
                R.layout.ads_search_list_recycler, container, false);
        RecyclerView rv = (RecyclerView) ln.findViewById(R.id.recyclerview);
        setupRecyclerView(rv);

        spinner = (Spinner) ln.findViewById(R.id.categorias_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.array_categorias, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new OnitemSelect());
        return ln;
    }

    @Override
    public void onResume() {
        super.onResume();
        com.danilo.vargas.adspoint.datos.Promociones promos =
                new com.danilo.vargas.adspoint.datos.Promociones(getActivity());
        promos.open();
        promociones = promos.ObtenerPromociones();
        promos.close();
        recargarPromociones(spinner.getSelectedItem().toString());
    }

    //Este metodo funciona para enviarle al recycleView la lista de promociones que queremos mostrar
    private void setupRecyclerView(RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        com.danilo.vargas.adspoint.datos.Promociones promos =
                new com.danilo.vargas.adspoint.datos.Promociones(getActivity());
        promos.open();
        promociones = promos.ObtenerPromociones();
        adapter = new SimpleStringRecyclerViewAdapter(getActivity(),
                promociones);
        recyclerView.setAdapter(adapter);
        promos.close();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //En este metodo desactivamos las promociones para que o parescan con el color de nuevo comentario
    //cuando el usuario vuelva a utilizar la aplicacion
    @Override
    public void onDestroy() {
        super.onDestroy();
        com.danilo.vargas.adspoint.datos.Promociones promos =
                new com.danilo.vargas.adspoint.datos.Promociones(getActivity());
        promos.open();
        promos.desactivar();
        promos.close();
    }

    private void recargarPromociones(String valorSeleccionado){
        ArrayList<Promocion> promos = new ArrayList<>();
        promos.addAll(promociones);
        Iterator<Promocion> i = promos.iterator();
        if (!valorSeleccionado.equals("Todas")){
            while (i.hasNext()) {
                Promocion s = i.next();
                if (!s.getmCategoria().equals(valorSeleccionado)){
                    i.remove();
                }
            }
            adapter.setmValues(promos);
            adapter.notifyDataSetChanged();
        } else {
            adapter.setmValues(promociones);
            adapter.notifyDataSetChanged();
        }
    }

    private class OnitemSelect implements AdapterView.OnItemSelectedListener {


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
           String tem = parent.getItemAtPosition(position).toString();
            recargarPromociones(tem);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    //Este es el adapter del RecyclerView que se encarga de pintar los items de la lista
    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private ArrayList<Promocion> mValues;
        private final Context mContext;

        public ArrayList<Promocion> getmValues() {
            return mValues;
        }

        public void setmValues(ArrayList<Promocion> mValues) {
            this.mValues = mValues;
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public String mLocalString;
            public String mDescripcionString;
            public String mTituloString;

            public final View mView;
            public final TextView mLocal;
            public final TextView mComentario;
            public final TextView mTitulo;
            public final CardView mCard;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mLocal = (TextView) view.findViewById(R.id.local);
                mComentario = (TextView) view.findViewById(R.id.descripcion);
                mTitulo = (TextView) view.findViewById(R.id.titulo);
                mCard = (CardView) view.findViewById(R.id.card);

            }

            @Override
            public String toString() {
                return super.toString() + " '" + mLocal.getText();
            }
        }

        public SimpleStringRecyclerViewAdapter(Context context, ArrayList<Promocion> items) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mValues = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_promociones, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final Promocion promocion =  mValues.get(position);
            holder.mLocalString = promocion.getmNombreLocal();
            holder.mDescripcionString = promocion.getMensaje();
            holder.mTituloString = promocion.getTitulo();
            holder.mLocal.setText(holder.mLocalString );
            holder.mComentario.setText(holder.mDescripcionString);
            holder.mTitulo.setText(holder.mTituloString);
            if (promocion.ismActvo()){
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.verde_claro));
            } else {
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.celeste));
            }

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, DetalleLocalActivity.class);
                    intent.putExtra(DetalleLocalActivity.EXTRA_NAME, promocion.getIdLocal());
                    context.startActivity(intent);

                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }
    }



}
