package com.danilo.vargas.adspoint;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.danilo.vargas.adspoint.Objetos.Grupo;
import com.danilo.vargas.adspoint.datos.Grupos;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;


public class NuevoGrupoActivity extends AppCompatActivity {

    public static int NO_OPTIONS=0;
    EditText edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_grupo);

        edit = (EditText) findViewById(R.id.edt);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nuevo_grupo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.ok){
            //En esta seccion de codigo guardamos el nuevo grupo si existen caracteres en input mombre de grupo
            //Ademas de crear el header para el grupo
            String header = header(edit.getText().toString());
            if (!header.equals("") &&
                    !edit.getText().toString().equals("")){

                Grupos grupos = new Grupos(this);
                grupos.open();
                grupos.guardarGrupo(new Grupo(0, header, edit.getText().toString()));
                grupos.close();
                finish();

            } else {

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.vulva_intentar)
                        .setTitle(R.string.title_activity_nuevo_grupo);
                builder.setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }

            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    //con este metodo generamos un header unico para nuestro grupo
    private String header(String pTexto) {

        MessageDigest digester;
        pTexto += new Date().getTime();
        try {
            digester = MessageDigest.getInstance("MD5");
            digester.update(pTexto.getBytes("ASCII"));
            byte[] digest = digester.digest();
            return convertToHex(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

        private static String convertToHex(byte[] data) throws java.io.IOException
        {

            StringBuilder sb = new StringBuilder();
            String hex;

            hex= Base64.encodeToString(data, 0, data.length, NO_OPTIONS);

            sb.append(hex);

            return sb.toString();
        }


    }

