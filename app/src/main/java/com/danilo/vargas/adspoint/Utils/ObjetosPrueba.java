package com.danilo.vargas.adspoint.Utils;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.Objetos.Grupo;
import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Objetos.Promocion;

import java.util.ArrayList;
import java.util.Date;

public class ObjetosPrueba {
    public static ArrayList<Promocion> Promociones() {

        ArrayList<Promocion> promociones = new ArrayList<>();






        return promociones;
    }

    public static ArrayList<Comentario> Comentarios() {

        ArrayList<Comentario> comentarios = new ArrayList<>();

        comentarios.add(new Comentario(0, "1", "Luis", "Pizza Deliciosa", ObtenerTimestamp.obtenerTimestamp(), "1","","", "", true));
        comentarios.add(new Comentario(0, "2", "Juan", "Zapatos Mala Calidad", ObtenerTimestamp.obtenerTimestamp(), "2","","", "", true));
        comentarios.add(new Comentario(0, "3", "Luis", "Exelente App", ObtenerTimestamp.obtenerTimestamp(), "","","", "", true));
        comentarios.add(new Comentario(0, "4", "Juan", "Vayan a jugar bingo esta buenisimo!!", ObtenerTimestamp.obtenerTimestamp(), "","2","", "", true));

        comentarios.add(new Comentario(0, "5", "Victor", "Pizaa delgada", ObtenerTimestamp.obtenerTimestamp(), "1","","", "", true));
        comentarios.add(new Comentario(0, "6", "Mora", "Zapatos espantosos", ObtenerTimestamp.obtenerTimestamp(), "2","","", "", true));
        comentarios.add(new Comentario(0, "7", "Danilo", "Hola amigos", ObtenerTimestamp.obtenerTimestamp(), "", "2" ,"", "", true));
        comentarios.add(new Comentario(0, "8", "Pichin", "Estoy en la pizzeria esta buenisima",
                new Date().toString(), "", "1", "", "", true));

        comentarios.add(new Comentario(0, "9", "Lui Carlo", "Hola!!", ObtenerTimestamp.obtenerTimestamp(), "", "2" ,"", "", true));
        comentarios.add(new Comentario(0, "10", "Hola Mundo", "Hola Mundo", ObtenerTimestamp.obtenerTimestamp(), "", "2" ,"", "", true));
        comentarios.add(new Comentario(0, "11", "Juanito Perez", "dmffmmfmfsfmsmdlñdsmmdfmsdsmsmsm" +
                "fsmddsmfsfmdsñmsdmfsñmldfdsñfmdsa{mfsñlmfdsñlmfdñslmf{smdfsdmsfdmsdfmmfdssdfmsdfms" +
                "sdfmsfdmñl osjigskjgjasgoajsdgoijsdgjsdjgsjdgjdsgjsajgasjsadjdgsajogdsojsag",
                ObtenerTimestamp.obtenerTimestamp(), "", "2" ,"", "", true));

        return comentarios;
    }

    public static ArrayList<Local> Locales() {

        ArrayList<Local> comentarios = new ArrayList<>();

        comentarios.add(new Local(0, "1", "Pizzeria la mejor", "8888888", "pizza@pizza.com",
                "La mejor pizza del mundo","Sucre"));
        comentarios.add(new Local(0, "2", "Zapateria Chuz", "33333333", "zapato@zapato.com",
                "Los mejores zapatos","Ciudad Quesada"));

        return comentarios;
    }

    public static ArrayList<Grupo> grupos() {

        ArrayList<Grupo> grupos = new ArrayList<>();
        grupos.add(new Grupo(0, "1", "Guaritos Sucre"));
        grupos.add(new Grupo(0, "2", "Guaritos"));

        return grupos;
    }
}
