package com.danilo.vargas.adspoint.servicio;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.IBinder;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.danilo.vargas.adspoint.MainActivity;
import com.danilo.vargas.adspoint.R;
import com.danilo.vargas.adspoint.conexion.AppController;
import com.danilo.vargas.adspoint.datos.Comentarios;
import com.danilo.vargas.adspoint.datos.Locales;
import com.danilo.vargas.adspoint.datos.Promociones;
import com.danilo.vargas.adspoint.logica.Json;
import com.danilo.vargas.adspoint.logica.VerificarDatosExistentes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyService extends Service {
    private static final String TAG = "HelloService";
    public static final int NOTIFICATION_ID = 1;
    public static final String URL_FORMAT = "http://192.168.1.29:8000/cliente";
    public static final String URL_FORMAT_TIME = "http://192.168.1.29:8000/tiempoRespuesta";
    public static final String CANTIDAD_FORMAT = "%s Promociones Nuevas";
    public static final String TEXTO_PROMO_FORMAT = "%s (%s)";
    private long mRequestStartTime;

    private boolean isRunning  = false;

    @Override
    public void onCreate() {
        Log.i(TAG, "Service onCreate");

        isRunning = true;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        verificarServicio(getApplicationContext(), intent);


        return Service.START_STICKY;
    }


    @Override
    public IBinder onBind(Intent arg0) {
        Log.i(TAG, "Service onBind");
        return null;
    }

    @Override
    public void onDestroy() {

        isRunning = false;

        Log.i(TAG, "Service onDestroy");
    }

    public void verificarServicio(final Context context, final Intent pIntent) {

        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        boolean isWiFi = false;

        if (activeNetwork != null){
            isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        }
        //validamos nuevamente nuestra conexon
        if (isWiFi) {

            //Tomamos la puerta de enlace para poder hacer nuestra consulta
            String url = URL_FORMAT;
            /*Abrimos las conexiones para la tablas y asi obtener todos los registros que poseemos
            en nuestra base de datos */
            Promociones promociones = new Promociones(context);
            Locales locales = new Locales(context);
            Comentarios comentarios = new Comentarios(context);
            promociones.open();
            locales.open();
            comentarios.open();

            /*En esta linea de codigo en la clase Json en en paquete logica generamos el JSONObject
            o json que le vamos a enviar al servidor por medio de de la peticion JsonObjectRequest
             */
            final JSONObject json = Json.crearJson(promociones.ObtenerPromociones(),
                    comentarios.ObtenerComentarios(),
                    locales.ObtenerLocales());

            //Cerramos las instancias a la base de datos
            promociones.close();
            locales.close();
            comentarios.close();

            //Realizamos la peticion al servidor utiizando JsonObjectRequest de la libreria Volley
            JsonObjectRequest jsonObjTestReq = new JsonObjectRequest(Request.Method.POST, URL_FORMAT_TIME, json,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            long totalRequestTime = System.currentTimeMillis() - mRequestStartTime;
                            try{
                                File  ruta_tarjeta = Environment.getExternalStorageDirectory();
                                File f = new File(ruta_tarjeta.getAbsolutePath(),"tiempoDeRequest.csv");

                                String date = new SimpleDateFormat("dd-MM-yyyy"
                                        + " HH:mm:ss").format(new Date());
                               String  text =  date +"; "+ totalRequestTime +"; "+ json.toString() + "\n" ;
                                OutputStreamWriter out  = new OutputStreamWriter(new FileOutputStream(f, true));
                                out.write(text);
                                out.flush();

                            }
                            catch (Exception ex){
                                Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Ficheros", error.toString());
                }
            });

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, json,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            //Validamos y verificamos nuestros datos con los que nos envio el servicio
                            VerificarDatosExistentes vf = new VerificarDatosExistentes(context);
                            vf.nuevasLocalesCliente(response);
                            vf.nuevosComentariosCliente(response);
                            //Si encontramos que hay nuevas promociones para nosotros notificamos al usuario
                            JSONArray nuevas = vf.nuevasPromocionesCliente(response);
                            if (nuevas.length() > 0){
                                notificarUsuario(context, nuevas, pIntent);
                            }


                            vf.close();

                            try {
                                if (MainActivity.getInstace() != null){
                                    MainActivity.getInstace().updateTheTextView();
                                }

                            } catch (Exception e) {

                            }
                            stopSelf();



                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //Si sucede un error no hacemos nada (posiblemente el host no tenga el servicio necesario)
                    Log.e("SmsReceiver", "Exception smsReceiver" + error);
                    stopSelf();
                }
            });
            mRequestStartTime = System.currentTimeMillis();
            AppController.getInstance().addToRequestQueue(jsonObjTestReq);
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        }
    }

    //En este metodo generamos las notificacion para el usuario
    private void notificarUsuario(Context pContext, JSONArray response, Intent pIntent) {

        PendingIntent pendingIntent;
        if (MainActivity.getInstace() == null){
            Log.i(TAG, "Es null");
            Intent intent = new Intent(pContext, MainActivity.class);
            pendingIntent = PendingIntent.getActivity(pContext, 0, intent, 0);
        } else {
            Log.i(TAG, "No Es null");
            pendingIntent = PendingIntent.getActivity(pContext, 0, pIntent, 0);
        }

        String nombreApp = pContext.getResources().getString(R.string.app_name);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(pContext);
        builder.setSmallIcon(R.mipmap.ic_logo);
        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(true);
        builder.setLargeIcon(BitmapFactory.decodeResource(pContext.getResources(), R.mipmap.ic_logo));
        builder.setContentTitle(nombreApp);
        builder.setContentText(nombreApp);
        builder.setDefaults(Notification.DEFAULT_SOUND);

        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle(nombreApp);
        for (int i = 0; i < response.length(); i++) {
            try {
                JSONObject person  = (JSONObject) response.get(i);

                inboxStyle.addLine(
                        String.format(TEXTO_PROMO_FORMAT, person.getString("titulo"),
                                person.getString("mensaje")));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        builder.setSubText(String.format(CANTIDAD_FORMAT, response.length()));
        builder.setStyle(inboxStyle);
        NotificationManager notificationManager = (NotificationManager) pContext.getSystemService(
                Context.NOTIFICATION_SERVICE);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }
}
