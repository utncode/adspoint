package com.danilo.vargas.adspoint.datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.danilo.vargas.adspoint.Objetos.Grupo;
import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Utils.ObtenerTimestamp;

import java.util.ArrayList;

public class Grupos {

    private static final int  ID = 0;
    private static final int HEADER = 1;
    private static final int NOMBRE = 2;
    private static String FORMATO_IGUAL = "%s = ?";

    private SQLiteDatabase database;
    private DBHelper dbHelper;

    private String[] allColumns = {
            DBHelper.GrupoDB.ID,
            DBHelper.GrupoDB.HEADER,
            DBHelper.GrupoDB.NOMBRE,
            DBHelper.GrupoDB.ACTIVO};

    public Grupos(Context context) {
        dbHelper = new DBHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Grupo guardarGrupo(Grupo pGrupo) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.GrupoDB.NOMBRE, pGrupo.getNombre());
        values.put(DBHelper.GrupoDB.HEADER, pGrupo.getHeader());
        values.put(DBHelper.GrupoDB.ACTIVO, true);
        values.put(DBHelper.CREATED_AT, ObtenerTimestamp.obtenerTimestamp());
        values.put(DBHelper.UPDATE_AT, ObtenerTimestamp.obtenerTimestamp());

        long insertId = database.insert(DBHelper.GrupoDB.NOMBRE_TABLA, null,
                values);
        pGrupo.setId(insertId);

        return pGrupo;
    }

    public void eliminarGrupo(Grupo pGrupo) {
        long id = pGrupo.getId();
        String sql = String.format(FORMATO_IGUAL, DBHelper.GrupoDB.ID);
        database.delete(DBHelper.GrupoDB.NOMBRE_TABLA, sql,
                new String[]{String.valueOf(id)});
    }

    public String obtenerNombreGrupo(String pIdgrupo){
        String nombre = "";
        if (pIdgrupo != null) {
            String sql = String.format(FORMATO_IGUAL, DBHelper.GrupoDB.HEADER);
            Cursor cursor = database.query(DBHelper.GrupoDB.NOMBRE_TABLA,
                    new String[]{DBHelper.GrupoDB.NOMBRE,}, sql, new String[]{pIdgrupo},
                    null, null, null, null);

            cursor.moveToFirst();
            if (cursor != null && cursor.moveToFirst()) {

                nombre = cursor.getString(ID);
            }
            cursor.close();
        }

        return nombre;
    }

    public ArrayList<Grupo> ObtenerGrupos(){
        ArrayList<Grupo> locales = new ArrayList();

        Cursor cursor = database.query(DBHelper.GrupoDB.NOMBRE_TABLA,
                allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            locales.add(new Grupo(cursor.getLong(ID), cursor.getString(HEADER),
                    cursor.getString(NOMBRE)));
            cursor.moveToNext();
        }

        cursor.close();

        return locales;
    }
}
