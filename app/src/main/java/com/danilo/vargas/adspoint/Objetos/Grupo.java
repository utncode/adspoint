package com.danilo.vargas.adspoint.Objetos;

import java.io.Serializable;

public class Grupo implements Serializable {
    private long mId;
    private String mHeader;
    private String mNombre;

    public Grupo(long pId, String pHeader, String pNombre) {
        mId = pId;
        mHeader = pHeader;
        mNombre = pNombre;
    }

    public long getId() {

        return mId;
    }

    public void setId(long pId) {

        mId = pId;
    }

    public String getHeader() {

        return mHeader;
    }

    public void setHeader(String pHeader) {

        mHeader = pHeader;
    }

    public String getNombre() {

        return mNombre;
    }

    public void setNombre(String pNombre) {
        mNombre = pNombre;
    }

    @Override
    public String toString() {
        return mNombre;
    }
}
