package com.danilo.vargas.adspoint;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.danilo.vargas.adspoint.fragments.Comentarios;
import com.danilo.vargas.adspoint.fragments.Promociones;
import com.danilo.vargas.adspoint.servicio.MyService;
import com.quinny898.library.persistentsearch.SearchBox;
import com.quinny898.library.persistentsearch.SearchResult;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    private static MainActivity ins = null;
    Adapter adapter;
    public static final String PREFS_NAME = "MyPrefsFile";
    public static MainActivity  getInstace(){
        return ins;
    }

    //validamos si el usuario ya acceso al sistema si no vamos a la pantalla de login
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        boolean logeado = settings.getBoolean("logeado", false);

        // si el usuario esta logeado cargamos la vista home o inicial si no la vist de login
        if (logeado){
            ins = this;
            cargarVistaInicial();
        } else {
            setContentView(R.layout.login);
        }
    }
    public void updateTheTextView() {
        adapter.getItem(0).onResume();
        adapter.getItem(1).onResume();
        MainActivity.this.runOnUiThread(new Runnable() {
            public void run() {

                adapter.getItem(0).onResume();
                adapter.getItem(1).onResume();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ins = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        ins = null;
    }

    private void cargarVistaInicial(){
        setContentView(R.layout.activity_main);
        final View view = findViewById(R.id.fab);
        view.setVisibility(View.GONE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        assert viewPager != null;
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.grupos) {
            Intent intent = new Intent(this, GruposActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //Cargamos la paginas de nestros tabs
    private void setupViewPager(ViewPager viewPager) {
        adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new Promociones(), "Promociones");
        adapter.addFragment(new Comentarios(), "Comentarios");
        viewPager.setAdapter(adapter);
    }

    class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    public void login(View view){
        EditText edtUsuario = (EditText) findViewById(R.id.usuario);
        if (!edtUsuario.getText().toString().equals("")){
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("logeado", true);
            editor.putString("usuario", edtUsuario.getText().toString());
            editor.apply();
            cargarVistaInicial();
        }
    }
}
