package com.danilo.vargas.adspoint.servicio;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.danilo.vargas.adspoint.MainActivity;
import com.danilo.vargas.adspoint.R;
import com.danilo.vargas.adspoint.conexion.AppController;
import com.danilo.vargas.adspoint.datos.Comentarios;
import com.danilo.vargas.adspoint.datos.Locales;
import com.danilo.vargas.adspoint.datos.Promociones;
import com.danilo.vargas.adspoint.logica.Json;
import com.danilo.vargas.adspoint.logica.VerificarDatosExistentes;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/* Esta clase dispara el metodo onReceive cuando hay un cambio de red en el telefono
* */

public class CambioStatusWifi extends BroadcastReceiver {


    /*En este metodo validamos que la conexion se wifi y no datos moviles, ademas de validar lo que lo
     disparo no fuera el apagado del wifi
     Si todo es correcto llamamos al metodo verificarServicio*/
    @Override
    public void onReceive(final Context context, Intent intent) {
        WifiManager wifiManager = (WifiManager)context.getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        List<ScanResult> results =  wifiManager.getScanResults();
        String networkSSID = "Cisco";
        Log.e("SmsReceiver", "Voy");
        int comprobacionIntervaloSegundos = 30;

        Intent intentS = new Intent(context, MyService.class);
        PendingIntent pintent = PendingIntent.getService(context, 0, intentS, 0);


        AlarmManager alarmManager = (AlarmManager)context.getSystemService(context.ALARM_SERVICE);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, 10);

        for( ScanResult result : results ) {
            if (result.SSID.equals(networkSSID)) {
                if (!wifiInfo.getSSID().equals("\"" + networkSSID + "\"")) {
                    WifiConfiguration conf = new WifiConfiguration();
                    conf.SSID = "\"" + networkSSID + "\"";
                    conf.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
                    wifiManager.addNetwork(conf);
                    List<WifiConfiguration> list = wifiManager.getConfiguredNetworks();
                    for (WifiConfiguration i : list) {
                        if (i.SSID != null && i.SSID.equals("\"" + networkSSID + "\"")) {
                            wifiManager.disconnect();
                            wifiManager.enableNetwork(i.networkId, true);
                            wifiManager.reconnect();


                            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), comprobacionIntervaloSegundos * 1000, pintent);
                            break;
                        }
                    }
                } else {
                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), comprobacionIntervaloSegundos * 1000, pintent);
                }
            }
        }
    }


}
