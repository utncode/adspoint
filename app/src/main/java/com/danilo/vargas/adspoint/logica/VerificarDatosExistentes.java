package com.danilo.vargas.adspoint.logica;

import android.content.Context;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Objetos.Promocion;
import com.danilo.vargas.adspoint.datos.Comentarios;
import com.danilo.vargas.adspoint.datos.Locales;
import com.danilo.vargas.adspoint.datos.Promociones;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class VerificarDatosExistentes {
    final Promociones mPromociones;
    final Locales mLocales;
    final Comentarios mComentarios;

    //abrimos las conexiones para obtener datos de la tablas par poder validar
    public VerificarDatosExistentes(Context pContext) {
        mPromociones = new Promociones(pContext);
        mLocales = new Locales(pContext);
        mComentarios = new Comentarios(pContext);
        mPromociones.open();
        mLocales.open();
        mComentarios.open();
    }

    public void close(){
        mPromociones.close();
        mLocales.close();
        mComentarios.close();
    }

    /*validamos las nuevas promociones que tenemos con las base de la datos y
    retornamos JSONArray con las nuevas  promociones para notificar al usuario,
    las nuevas las guardamos en la base de datos
     */
    public JSONArray nuevasPromocionesCliente(JSONObject response){
        JSONArray obj = new JSONArray();
        JSONObject body = new JSONObject();
        JSONArray retorno = new JSONArray();
        ArrayList<String> headers = mPromociones.ObtenerHeaders();
        try {
            body = response.getJSONObject(Json.BODY);
            obj = body.getJSONArray(Json.PROMOCIONES_PARENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject promocion;
        boolean existe ;
        for (int i = 0; i < obj.length(); i++) {
            try {
                existe = false;
                promocion = (JSONObject) obj.get(i);
                String header2 = promocion.getString("header");
                for( String header : headers ){

                    if (header.equals(header2))
                        existe = true;
                }
                if (!existe) {
                    mPromociones.guardarPromocion(new Promocion(0, promocion.getString(Json.HEADER),
                            promocion.getString(Json.TITULO), promocion.getString(Json.LOCAL),
                            promocion.getString(Json.FECHA_EXPIRACION),
                            promocion.getString(Json.MENSAJE),
                            "", true,
                            promocion.getString(Json.CATEGORIA)));
                    retorno.put(promocion);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return retorno;
    }

    public ArrayList<Promocion> nuevasPromocionesServidor(JSONObject response){
        JSONArray obj = new JSONArray();
        ArrayList<Promocion> promociones = mPromociones.ObtenerPromociones();


        try {
            obj = response.getJSONArray(Json.PROMOCIONES_PARENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject promocion;


        for (Iterator<Promocion> iterator = promociones.iterator(); iterator.hasNext(); ) {
            Promocion px = iterator.next();
            for (int i = 0; i < obj.length(); i++) {
                try {

                    promocion = (JSONObject) obj.get(i);

                    if (promocion.getString(Json.HEADER).equals(px.getHeader()))
                        iterator.remove();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return promociones;
    }

    //validamos los nuevos locales que tenemos con los que hay base de datos y si no existe los guardamos

    public void nuevasLocalesCliente(JSONObject response){
        JSONArray obj = new JSONArray();
        JSONObject body = new JSONObject();
        ArrayList<String> headers = mLocales.ObtenerHeaders();
        try {
            body = response.getJSONObject(Json.BODY);
            obj = body.getJSONArray(Json.LOCALES_PARENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject local;
        boolean existe ;
        for (int i = 0; i < obj.length(); i++) {
            try {
                existe = false;
                local = (JSONObject) obj.get(i);
                for( String header : headers ){
                    if (header.equals(local.getString(Json.HEADER)))
                        existe = true;
                }
                if (!existe) {
                    mLocales.guardarPromocion(new Local(0, local.getString(Json.HEADER),
                            local.getString(Json.NOMBRE), local.getString(Json.TELEFONO),
                            local.getString(Json.CORREO), local.getString(Json.DESCRIPCION),
                            local.getString(Json.UBICACION)));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Local> nuevasLocalesServidor(JSONObject response){
        JSONArray obj = new JSONArray();
        ArrayList<Local> promociones = mLocales.ObtenerLocales();


        try {
            obj = response.getJSONArray(Json.LOCALES_PARENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject promocion;


        for (Iterator<Local> iterator = promociones.iterator(); iterator.hasNext(); ) {
            Local px = iterator.next();
            for (int i = 0; i < obj.length(); i++) {
                try {

                    promocion = (JSONObject) obj.get(i);

                    if (promocion.getString(Json.HEADER).equals(px.getmHeader()))
                    {
                        iterator.remove();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return promociones;
    }

    //validamos los nuevos comentarios que tenemos con los que hay base de datos y si no existe los guardamos
    public void nuevosComentariosCliente(JSONObject response){
        JSONArray obj = new JSONArray();
        JSONObject body = new JSONObject();
        ArrayList<String> headers = mComentarios.ObtenerHeaders();
        try {
            body = response.getJSONObject(Json.BODY);
            obj = body.getJSONArray(Json.COMENTARIOS_PARENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject comentario;
        boolean existe ;
        for (int i = 0; i < obj.length(); i++) {
            try {
                existe = false;
                comentario = (JSONObject) obj.get(i);
                String header2 = comentario.getString("header");
                for( String header : headers ){
                    if (header.equals(header2))
                        existe = true;
                }
                if (!existe) {
                    mComentarios.crearComentario(new Comentario(0,
                            comentario.getString(Json.HEADER),
                            comentario.getString(Json.USUARIO),
                            comentario.getString(Json.COMENTARIO),
                            comentario.getString(Json.FECHA),
                            comentario.getString(Json.LOCAL),
                            comentario.getString(Json.GRUPO),
                            "",
                            "",
                            true));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Comentario> nuevasComentariosServidor(JSONObject response){
        JSONArray obj = new JSONArray();
        ArrayList<Comentario> comentarios = mComentarios.ObtenerComentarios();


        try {
            obj = response.getJSONArray(Json.COMENTARIOS_PARENT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JSONObject promocion;


        for (Iterator<Comentario> iterator = comentarios.iterator(); iterator.hasNext(); ) {
            Comentario px = iterator.next();
            for (int i = 0; i < obj.length(); i++) {
                try {

                    promocion = (JSONObject) obj.get(i);

                    if (promocion.getString(Json.HEADER).equals(px.getHeader()))
                        iterator.remove();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        return comentarios;
    }
}
