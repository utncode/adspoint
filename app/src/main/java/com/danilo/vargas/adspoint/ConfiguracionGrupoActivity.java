package com.danilo.vargas.adspoint;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.danilo.vargas.adspoint.Objetos.Grupo;
import com.danilo.vargas.adspoint.datos.Grupos;


public class ConfiguracionGrupoActivity extends AppCompatActivity {

    //Esta vista no permite agragr un integrante al grupo o salir del grupo
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion_grupo);
       Grupo grupo =  (Grupo) getIntent().getSerializableExtra("grupo");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(grupo.getNombre());
        final ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(grupo.getNombre());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grupos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //eliminamos el grupo de la base de datos
    public void salirGrupo(View view ){
        Grupo grupo =  (Grupo) getIntent().getSerializableExtra("grupo");
        Grupos grupos = new Grupos(this);
        grupos.open();
        grupos.eliminarGrupo(grupo);
        grupos.close();
        finish();
    }

    //Enviamos un nuevo mensaje con el formato para crear un grupo
    public void enviarGrupo(View view) {
        try {
            // Get the default instance of the SmsManager
            Grupo grupo =  (Grupo) getIntent().getSerializableExtra("grupo");
            String cuerpo = "AdsPoint\n" + grupo.getHeader()  + grupo.getNombre();
            EditText et = (EditText) findViewById(R.id.edtNumero);
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(et.getText().toString(),
                    null,
                    cuerpo,
                    null,
                    null);
            Toast.makeText(getApplicationContext(), "Se ha enviado la solicitud",
                    Toast.LENGTH_LONG).show();
            et.setText("");
            View viewFocus = this.getCurrentFocus();
            if (viewFocus != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(),"Your sms has failed...",
                    Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
    }
}
