package com.danilo.vargas.adspoint.Objetos;

public class Promocion{
    private long mId;
    private String mHeader;
    private String mTitulo;
    private String mIdLocal;
    private String mfechaCaducidad;
    private String mMensaje;
    private String mNombreLocal;
    private String mCategoria;
    private boolean mActvo;

    public Promocion(long pId, String pHeader, String pTitulo, String pIdLocal,
                     String pfechaCaducidad, String pMensaje, String pNombreLocal,
                     boolean pActivo, String pCategoria) {
        this.mId = pId;
        this.mHeader = pHeader;
        this.mTitulo = pTitulo;
        this.mIdLocal = pIdLocal;
        this.mfechaCaducidad = pfechaCaducidad;
        this.mMensaje = pMensaje;
        mNombreLocal = pNombreLocal;
        mActvo = pActivo;
        mCategoria = pCategoria;
    }

    public boolean ismActvo() {
        return mActvo;
    }

    public void setmActvo(boolean mActvo) {
        this.mActvo = mActvo;
    }

    public String getmNombreLocal() {
        return mNombreLocal;
    }

    public void setmNombreLocal(String mNombreLocal) {
        this.mNombreLocal = mNombreLocal;
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public String getHeader() {
        return mHeader;
    }

    public void setHeader(String mHeader) {
        this.mHeader = mHeader;
    }

    public String getTitulo() {
        return mTitulo;
    }

    public void setTitulo(String mTitulo) {
        this.mTitulo = mTitulo;
    }

    public String getIdLocal() {
        return mIdLocal;
    }

    public void setIdLocal(String mIdLocal) {
        this.mIdLocal = mIdLocal;
    }

    public String getfechaCaducidad() {
        return mfechaCaducidad;
    }

    public void setfechaCaducidad(String mfechaCaducidad) {
        this.mfechaCaducidad = mfechaCaducidad;
    }

    public String getmCategoria() {
        return mCategoria;
    }

    public void setmCategoria(String mCategoria) {
        this.mCategoria = mCategoria;
    }

    public String getMensaje() {
        return mMensaje;
    }

    public void setMensaje(String mMensaje) {
        this.mMensaje = mMensaje;
    }
}
