package com.danilo.vargas.adspoint;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.Objetos.Grupo;

import java.util.ArrayList;

/*
* En esta vista vamos a ver el detalle de los grupos mostrando en una lista los comentario que tiene
* */
public class DetalleGrupoActivity extends AppCompatActivity {
    Grupo grupo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detall_grupo);
        grupo =  (Grupo) getIntent().getSerializableExtra("grupo");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(grupo.getNombre());
        final ActionBar ab = getSupportActionBar();
        assert ab != null;
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setTitle(grupo.getNombre());

        RecyclerView rv = (RecyclerView) findViewById(R.id.lista);
        setupRecyclerView(rv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_grupos, menu);
        return true;
    }
//Cargamos el adapter en recyclerView para mostrarlos en una lista
    private void setupRecyclerView(RecyclerView recyclerView) {
        //obtenermos los comentarios que se han realizado en ese grupo para luego enviarlos al adapter para que los muestre
        com.danilo.vargas.adspoint.datos.Comentarios comentarios =
                new com.danilo.vargas.adspoint.datos.Comentarios(this);
        comentarios.open();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new SimpleStringRecyclerViewAdapter(this,
                comentarios.obtenerComentariosGrupo(grupo.getHeader())));
        comentarios.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private ArrayList<Comentario> mValues;
        private final Context mContext;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public String mUsuarioString;
            public String mComentarioString;

            public final View mView;
            public final TextView mNombre;
            public final TextView mComentario;
            public final TextView mGrupo;
            public final CardView mCard;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mNombre = (TextView) view.findViewById(R.id.usuario);
                mComentario = (TextView) view.findViewById(R.id.comentario);
                mCard = (CardView) view.findViewById(R.id.card);
                mGrupo = (TextView) view.findViewById(R.id.grupo);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNombre.getText();
            }
        }

        public SimpleStringRecyclerViewAdapter(Context context, ArrayList<Comentario> items) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mValues = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_comentarios, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Comentario comentario = mValues.get(position);
            String grupoOLocal = !comentario.getmNombreLocal().equals("")?
                    comentario.getmNombreLocal():comentario.getmNombreGrupo();
            holder.mUsuarioString = comentario.getUsuario() +"@" + grupoOLocal;
            holder.mComentarioString = comentario.getComentario();
            holder.mNombre.setText(holder.mUsuarioString );
            holder.mComentario.setText(holder.mComentarioString);
            holder.mGrupo.setText(comentario.getmNombreGrupo());
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.verde_claro));
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }
    }

    //metodo que responde al click del boton configuracion
    public void configuracion(View view){
        Intent intent = new Intent(this, ConfiguracionGrupoActivity.class);
        intent.putExtra("grupo",grupo);
        startActivity(intent);
    }
}
