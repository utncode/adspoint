package com.danilo.vargas.adspoint;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.Objetos.Grupo;
import com.danilo.vargas.adspoint.Objetos.Local;
import com.danilo.vargas.adspoint.Utils.ObtenerTimestamp;
import com.danilo.vargas.adspoint.datos.Comentarios;
import com.danilo.vargas.adspoint.datos.Grupos;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;

/*
    Esta vista nos permita agregar un nuevo comentario para una tienda
    (Los comentarios pueden ser publicos o para un grupo)
 */
public class ComentarioNuevoActivity extends AppCompatActivity {
    public static int NO_OPTIONS=0;
    ArrayAdapter<Grupo> adapter;
    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comentario_nuevo);
        lista = (ListView) findViewById(R.id.listView);



        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        RadioButton publico = (RadioButton) findViewById(R.id.publico);
        publico.setChecked(true);
        Grupos grupos = new Grupos(this);
        grupos.open();
        adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_single_choice,
                android.R.id.text1, grupos.ObtenerGrupos());
        grupos.close();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void activarListView(View view){

        ListView lista = (ListView) findViewById(R.id.listView);
        lista.setAdapter(adapter);

    }

    public void desactivarListView(View view){
        ListView lista = (ListView) findViewById(R.id.listView);
        lista.setAdapter(null);
    }

    //Guardamos el nuevo comentario en la base de datos
    public void guardarComentario(View view){
        ListView lista = (ListView) findViewById(R.id.listView);
        Local local = (Local)  getIntent().getSerializableExtra("local");
        Comentarios comentarios = new Comentarios(this);
        comentarios.open();
        RadioButton publico = (RadioButton) findViewById(R.id.publico);
        SharedPreferences settings = getSharedPreferences(MainActivity.PREFS_NAME, 0);
        String usuario = settings.getString("usuario", "usuario");
        EditText edtComentario = (EditText) findViewById(R.id.edtComentario);
        String header = header(usuario + local.getmNombre());
        if (publico.isChecked()){

            comentarios.crearComentario(new Comentario(0, header, usuario,
                    edtComentario.getText().toString(), ObtenerTimestamp.obtenerTimestamp(),
                    local.getmHeader(), null, null, local.getmNombre(), true));
        } else {
           Grupo grupo =  adapter.getItem(lista.getCheckedItemPosition());
            comentarios.crearComentario(new Comentario(0, header, usuario,
                    edtComentario.getText().toString(), ObtenerTimestamp.obtenerTimestamp(),
                    local.getmHeader(), grupo.getHeader(), grupo.getNombre(), local.getmNombre(),
                    true));
        }

        finish();
    }

    //Generamos el MD5 para el comentario
    private String header(String pTexto) {

        MessageDigest digester;
        pTexto += new Date().getTime();
        try {
            digester = MessageDigest.getInstance("MD5");
            digester.update(pTexto.getBytes("ASCII"));
            byte[] digest = digester.digest();
            return convertToHex(digest);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

    private static String convertToHex(byte[] data) throws java.io.IOException
    {

        StringBuilder sb = new StringBuilder();

        String hex= Base64.encodeToString(data, 0, data.length, NO_OPTIONS);

        sb.append(hex);


        return sb.toString();
    }
}
