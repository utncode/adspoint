package com.danilo.vargas.adspoint.Objetos;

public class Comentario {
    private long mId;
    private String mHeader;
    private String mUsuario;
    private String mComentario;
    private String mFecha;
    private String mIdLocal;
    private String mIdGrupo;
    private String mNombreGrupo;
    private String mNombreLocal;
    private boolean mActivo;

    public Comentario(long pId, String pHeader, String pUsuario, String pComentario, String pFecha,
                      String pIdLocal, String pIdGrupo, String pNombreGrupo, String pNombreLocal,
                      boolean pActivo) {
        mId = pId;
        mHeader = pHeader;
        mUsuario = pUsuario;
        mComentario = pComentario;
        mFecha = pFecha;
        mIdLocal = pIdLocal;
        mIdGrupo = pIdGrupo;
        mNombreGrupo = pNombreGrupo;
        mNombreLocal = pNombreLocal;
        mActivo = pActivo;

    }

    public boolean ismActivo() {
        return mActivo;
    }

    public void setmActivo(boolean mActivo) {
        this.mActivo = mActivo;
    }

    public String getmNombreLocal() {
        return mNombreLocal;
    }

    public void setmNombreLocal(String mNombreLocal) {
        this.mNombreLocal = mNombreLocal;
    }

    public String getmNombreGrupo() {
        return mNombreGrupo;
    }

    public void setmNombreGrupo(String mNombreGrupo) {
        this.mNombreGrupo = mNombreGrupo;
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public String getHeader() {
        return mHeader;
    }

    public void setHeader(String mHeader) {
        this.mHeader = mHeader;
    }

    public String getUsuario() {
        return mUsuario;
    }

    public void setUsuario(String mUsuario) {
        this.mUsuario = mUsuario;
    }

    public String getComentario() {
        return mComentario;
    }

    public void setComentario(String mComentario) {
        this.mComentario = mComentario;
    }

    public String getFecha() {
        return mFecha;
    }

    public void setFecha(String mFecha) {
        this.mFecha = mFecha;
    }

    public String getIdLocal() {
        return mIdLocal;
    }

    public void setIdLocal(String mIdLocal) {
        this.mIdLocal = mIdLocal;
    }

    public String getmIdGrupo() {
        return mIdGrupo;
    }

    public void setmIdGrupo(String mIdGrupo) {
        this.mIdGrupo = mIdGrupo;
    }
}
