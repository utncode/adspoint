package com.danilo.vargas.adspoint.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.R;

import java.util.ArrayList;

public class Comentarios extends Fragment {
    /*
        Esta clase es una vista dentro del activity donde cargamos los comentarios en la
        vista por medio de una lista que esta alojada en el RecyclerView
    * */
    RecyclerView rv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rv = (RecyclerView) inflater.inflate(
                R.layout.fragment_list_recycler, container, false);



        return rv;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupRecyclerView(rv);
    }

    //Este metodo funciona para enviarle al recycleView la lista de comentarios que queremos mostrar
    private void setupRecyclerView(RecyclerView recyclerView) {
        com.danilo.vargas.adspoint.datos.Comentarios comentarios =
                new com.danilo.vargas.adspoint.datos.Comentarios(getActivity());
        comentarios.open();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(new SimpleStringRecyclerViewAdapter(getActivity(),
                comentarios.ObtenerComentarios()));
        comentarios.close();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    //En este metodo desactivamos los comentarios para que o parescan con el color de nuevo comentario
    //cuando el usuario vuelva a utilizar la aplicacion
    @Override
    public void onDestroy() {
        super.onDestroy();
        com.danilo.vargas.adspoint.datos.Comentarios comentarios =
                new com.danilo.vargas.adspoint.datos.Comentarios(getActivity());
        comentarios.open();
        comentarios.desactivar();
        comentarios.close();
    }

    //Este es el adapter del RecyclerView que se encarga de pintar los items de la lista
    public static class SimpleStringRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleStringRecyclerViewAdapter.ViewHolder> {

        private final TypedValue mTypedValue = new TypedValue();
        private int mBackground;
        private ArrayList<Comentario> mValues;
        private final Context mContext;

        public static class ViewHolder extends RecyclerView.ViewHolder {
            public String mUsuarioString;
            public String mComentarioString;

            public final View mView;
            public final TextView mNombre;
            public final TextView mComentario;
            public final TextView mGrupo;
            public final CardView mCard;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mNombre = (TextView) view.findViewById(R.id.usuario);
                mComentario = (TextView) view.findViewById(R.id.comentario);
                mCard = (CardView) view.findViewById(R.id.card);
                mGrupo = (TextView) view.findViewById(R.id.grupo);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNombre.getText();
            }
        }

        public SimpleStringRecyclerViewAdapter(Context context, ArrayList<Comentario> items) {
            context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
            mBackground = mTypedValue.resourceId;
            mValues = items;
            mContext = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.row_comentarios, parent, false);
            view.setBackgroundResource(mBackground);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            Comentario comentario = mValues.get(position);
            String grupoOLocal = !comentario.getmNombreLocal().equals("")?
                    comentario.getmNombreLocal():comentario.getmNombreGrupo();
            holder.mUsuarioString = comentario.getUsuario() +"@" + grupoOLocal;
            holder.mComentarioString = comentario.getComentario();
            holder.mNombre.setText(holder.mUsuarioString );
            holder.mComentario.setText(holder.mComentarioString);
            holder.mGrupo.setText(comentario.getmNombreGrupo());
            if (comentario.ismActivo()){
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.verde_claro));
            } else {
                holder.mCard.setCardBackgroundColor(
                        mContext.getResources().getColor(R.color.celeste));
            }
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }
    }

}
