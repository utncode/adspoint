package com.danilo.vargas.adspoint.datos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.danilo.vargas.adspoint.Objetos.Comentario;
import com.danilo.vargas.adspoint.Utils.ObtenerTimestamp;

import java.util.ArrayList;

public class Comentarios {
    /*Posiciones de las columnas de la tabla se basa en el arreglo allColumn
    esta posiciones la utilizamos en el metodo cursorComentario()
     */

    private static final int  ID = 0;
    private static final int HEADER = 1;
    private static final int USUARIO = 2;
    private static final int COMENTARIO = 3;
    private static final int FECHA = 4;
    private static final int ID_LOCAL = 5;
    private static final int ID_GRUPO = 6;
    private static final int ACTIVO = 7;
    //Instacia del ORM que permite manejar sqlite en el telefono
    private SQLiteDatabase database;
    private DBHelper dbHelper;
    private static String FORMATO_IGUAL = "%s = ?";
    private String[] allColumns = {
            DBHelper.ComentarioBD.ID,
            DBHelper.ComentarioBD.HEADER,
            DBHelper.ComentarioBD.USUARIO,
            DBHelper.ComentarioBD.COMENTARIO,
            DBHelper.ComentarioBD.FECHA,
            DBHelper.ComentarioBD.ID_LOCAL,
            DBHelper.ComentarioBD.ID_GRUPO,
            DBHelper.ComentarioBD.ACTIVO};
    private final Context mContext;

    //instanciamos el dbhelper para tener accseo a la base de fdatos luego
    public Comentarios(Context context) {
        dbHelper = new DBHelper(context);
        mContext = context;
    }

    //abrimos conexion a la base de datos
    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }


    //guardamos un nuevo comentario en la tabla comentario
    public Comentario crearComentario(Comentario pComentario) {
        ContentValues values = new ContentValues();
        values.put(DBHelper.ComentarioBD.ID_GRUPO, pComentario.getmIdGrupo());
        values.put(DBHelper.ComentarioBD.ID_LOCAL, pComentario.getIdLocal());
        values.put(DBHelper.ComentarioBD.HEADER, pComentario.getHeader());
        values.put(DBHelper.ComentarioBD.USUARIO, pComentario.getUsuario());
        values.put(DBHelper.ComentarioBD.COMENTARIO, pComentario.getComentario());
        values.put(DBHelper.ComentarioBD.FECHA, pComentario.getFecha());
        values.put(DBHelper.ComentarioBD.ACTIVO, true);
        values.put(DBHelper.CREATED_AT, ObtenerTimestamp.obtenerTimestamp());
        values.put(DBHelper.UPDATE_AT, ObtenerTimestamp.obtenerTimestamp());

        long insertId = database.insert(DBHelper.ComentarioBD.NOMBRE_TABLA, null,
                values);
        pComentario.setId(insertId);

        return pComentario;
    }


    public ArrayList<Comentario> ObtenerComentarios(){
        ArrayList<Comentario> comentarios = new ArrayList<>();


        String sortOrder =
                DBHelper.PromocionDB.ID + " DESC";
        Cursor cursor = database.query(DBHelper.ComentarioBD.NOMBRE_TABLA,
                allColumns, null, null, null, null, sortOrder,null);

        Locales locales = new Locales(mContext);
        Grupos grupos = new Grupos(mContext);
        locales.open();
        grupos.open();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            comentarios.add(cursorComentario(cursor, locales, grupos));
            cursor.moveToNext();
        }

        cursor.close();
        locales.close();
        grupos.close();
        return comentarios;
    }

    public ArrayList<String> ObtenerHeaders(){
        ArrayList<String> headers = new ArrayList<>();
        Cursor cursor = database.query(DBHelper.ComentarioBD.NOMBRE_TABLA,
                new String[]{DBHelper.ComentarioBD.HEADER}, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            headers.add(cursor.getString(cursor.getColumnIndex(DBHelper.ComentarioBD.HEADER)));
            cursor.moveToNext();
        }

        cursor.close();

        return headers;
    }

    //pasamos del cursor que nos devolvio la consulta a un objeto Comentario
    private Comentario cursorComentario(Cursor pCursor, Locales pLocales, Grupos pGrupos){
        String local = pLocales.obtenerNombreLocal(pCursor.getString(ID_LOCAL));
        String grupo = pGrupos.obtenerNombreGrupo(pCursor.getString(ID_GRUPO));
        boolean activo = pCursor.getInt(ACTIVO) == 1 ? true : false;
        return  new Comentario(pCursor.getLong(ID), pCursor.getString(HEADER),
                pCursor.getString(USUARIO), pCursor.getString(COMENTARIO),
                pCursor.getString(FECHA), pCursor.getString(ID_LOCAL), pCursor.getString(ID_GRUPO),
                grupo,local, activo);
    }

    public ArrayList<Comentario> obtenerComentariosGrupo(String pIdgrupo){
        ArrayList<Comentario> comentarios = new ArrayList();
        if (pIdgrupo != null) {
            String sql = String.format(FORMATO_IGUAL, DBHelper.ComentarioBD.ID_GRUPO);
            Cursor cursor = database.query(DBHelper.ComentarioBD.NOMBRE_TABLA,
                    allColumns, sql, new String[]{pIdgrupo},
                    null, null, null, null);
            Locales locales = new Locales(mContext);
            Grupos grupos = new Grupos(mContext);
            locales.open();
            grupos.open();

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                comentarios.add(cursorComentario(cursor, locales, grupos));
                cursor.moveToNext();
            }

            cursor.close();
            locales.close();
            grupos.close();
        }


        return comentarios;
    }

    public void desactivar(){
        ContentValues values = new ContentValues();
        values.put(DBHelper.ComentarioBD.ACTIVO, false);
                database.update(DBHelper.ComentarioBD.NOMBRE_TABLA, values, null, null);
    }
}